#!/usr/bin/env python3
"""
This module has utilities to parse input, write output, and solve the
Collatz Problem specified by this project, where we are looking to find the
maximum time it takes to get to 1 from any number over some range using the
procedure specified by the Collatz Conjecture.
"""

# ----------
# Collatz.py
# ----------

from typing import IO, List

MAX_INPUT_NUM = 1000000

# ------------
# collatz_read
# ------------


def collatz_read(line: str) -> List[int]:
    """
    read two ints

    line a string containing two ints that are space separated
    return a list of two ints, representing the beginning and end of a range
    """
    parsed_ints = list(map(int, line.split()))
    return [parsed_ints[0], parsed_ints[1]]

# ------------------
# init_collatz_table
# ------------------


def init_collatz_table() -> List[int]:
    """
    return table that stores all cycle lengths for possible input values
    """
    memo = [0 for i in range(MAX_INPUT_NUM + 1)]
    memo[1] = 1
    for i in range(1, MAX_INPUT_NUM + 1):
        collatz_run(memo, i)
    return memo

# -----------
# collatz_run
# -----------


def collatz_run(memo: List[int], num: int) -> int:
    """
    find the cycle length of a given number

    memo the current table of memoized cycle lengths that will be added to
    num the number to find the cycle length of
    return the cycle length of num
    """
    assert num > 0
    assert num <= MAX_INPUT_NUM
    # contains a list of the path we took to get to a known number in our
    # memoized table, where each element also tells if we took one or two steps
    # from the previous element.
    cycle_visited = []
    while num > MAX_INPUT_NUM or memo[num] == 0:
        if num & 1 == 0:  # check the last bit to see if divisible by 2
            cycle_visited.append((num, 1))
            num = num >> 1  # equivalent to dividing by 2
        else:
            # if it is odd, then we know that num = 2k + 1 for some k in
            # 1, 2, ... and thus doing (3 * num + 1)/2 is (3(2k + 1)+1)/2
            # = (2k+1) + k + 1 where k = floor(num/2). Therefore we can take
            # two steps (since we will never multiply by 3 and add 1 to get 1)
            # and do this together
            cycle_visited.append((num, 2))  # we are now taking two steps
            num = num + (num >> 1) + 1  # num >> 1 = floor(num/2)

    current_cycle_length = memo[num]
    # We know the cycle length of every element along the visited list
    # so we can add them all to our memoization table.
    while cycle_visited:
        next_num, steps = cycle_visited.pop()
        current_cycle_length += steps
        if next_num <= MAX_INPUT_NUM:
            memo[next_num] = current_cycle_length
    assert current_cycle_length > 0
    return current_cycle_length


# ------------
# collatz_eval
# ------------

def collatz_eval(memo: List[int], i: int, j: int) -> int:
    """
    Find the maximum cycle length between i and j

    memo the memoized table of cycle lengths for all numbers (already filled out)
    i one endpoint of the range to search over
    j the other endpoint of the range
    return the max cycle length of the range between i and j
    """
    beg = min(i, j)
    end = max(i, j)
    assert beg > 0
    assert end <= MAX_INPUT_NUM
    max_cycle_length = 1
    for current in range(beg, end + 1):
        cycle_length = memo[current]
        max_cycle_length = max(max_cycle_length, cycle_length)
    assert max_cycle_length > 0
    return max_cycle_length

# -------------
# collatz_print
# -------------


def collatz_print(writer: IO[str], i: int, j: int, max_cycle_length: int) -> None:
    """
    print three ints
    writer a writer
    i the first endpoint of the range to print
    j the second endpoint of the range to print
    max_cycle_length the max cycle length
    """
    assert i > 0
    assert max_cycle_length > 0
    writer.write(str(i) + " " + str(j) + " " + str(max_cycle_length) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(reader: IO[str], writer: IO[str]) -> None:
    """
    reader a reader
    writer a writer
    """
    memo = init_collatz_table()
    for line in reader:
        i, j = collatz_read(line)
        max_cycle_length = collatz_eval(memo, i, j)
        collatz_print(writer, i, j, max_cycle_length)
