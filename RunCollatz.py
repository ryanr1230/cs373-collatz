#!/usr/bin/env python3
"""
Run harness for the Collatz module using stdin and stdout
"""

# -------------
# RunCollatz.py
# -------------

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
