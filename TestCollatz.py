#!/usr/bin/env python3
"""
Test harness and tests for Collatz module
"""

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve
from Collatz import init_collatz_table, collatz_run

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """ Unit test class """

    @classmethod
    def setUpClass(cls):
        """ Initialize full table for multiple tests """
        cls._memo = init_collatz_table()

    @staticmethod
    def create_empty_memo_table():
        """ Create empty memo table for collatz_run """
        memo = [0 for i in range(1000000 + 1)]
        memo[1] = 1
        return memo

    # ----
    # read
    # ----

    def test_read(self):
        """ Test basic reading in """
        line = "1 10\n"
        i, j = collatz_read(line)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_more_integers(self):
        """ Test if line contains more integers than expected """
        line = "1 10 20\n"
        i, j = collatz_read(line)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ---
    # run
    # ---

    def test_run_simple(self):
        """ Tests cycle length of a single known value """
        max_cycle = collatz_run(TestCollatz.create_empty_memo_table(), 5)
        self.assertEqual(max_cycle, 6)

    def test_run_just_one(self):
        """ Tests cycle length starting at 1"""
        max_cycle = collatz_run(TestCollatz.create_empty_memo_table(), 1)
        self.assertEqual(max_cycle, 1)

    def test_run_power_of_2(self):
        """ Tests cycle length of powers of 2"""
        for i in range(0, 20):  # 2^19 is last power of 2 less than 10^6
            value = 2**i
            max_cycle = collatz_run(
                TestCollatz.create_empty_memo_table(), value)
            self.assertEqual(max_cycle, i + 1)
    # ----
    # eval
    # ----

    def test_eval_inclusive(self):
        """ Tests that eval range is inclusive """
        memo = [i for i in range(10)]
        max_cycle = collatz_eval(memo, 5, 5)
        self.assertEqual(max_cycle, 5)

    def test_eval_1(self):
        """ Test small interval """
        max_cycle = collatz_eval(self._memo, 1, 10)
        self.assertEqual(max_cycle, 20)

    def test_eval_1_backwards(self):
        """ Test small interval with the endpoints flipped """
        max_cycle = collatz_eval(self._memo, 10, 1)
        self.assertEqual(max_cycle, 20)

    def test_eval_2(self):
        """ Test medium interval """
        max_cycle = collatz_eval(self._memo, 100, 200)
        self.assertEqual(max_cycle, 125)

    def test_eval_3(self):
        """ Test small interval with larger numbers """
        max_cycle = collatz_eval(self._memo, 201, 210)
        self.assertEqual(max_cycle, 89)

    def test_eval_4(self):
        """ Test large interval """
        max_cycle = collatz_eval(self._memo, 1, 1000000)
        self.assertEqual(max_cycle, 525)

    # -----
    # print
    # -----

    def test_print(self):
        """ Test collatz_print basic functionality """
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        self.assertEqual(writer.getvalue(), "1 10 20\n")

    def test_print_multiple_lines(self):
        """
        Test that collatz_print writes multiple times to the same writer
        and behaves well
        """
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        collatz_print(writer, 2, 9, 20)
        self.assertEqual(writer.getvalue(), "1 10 20\n2 9 20\n")

    # -----
    # solve
    # -----

    def test_solve_empty_string(self):
        """ Test that an empty input does not cause problems with module """
        reader = StringIO()
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(writer.getvalue(), "")

    def test_solve(self):
        """ Test entire thing over basic intervals from before """
        reader = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(
            writer.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_original_order(self):
        """
        Test that collatz_solve prints endpoints in original order, even if
        j < i
        """
        reader = StringIO("10 1\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(
            writer.getvalue(), "10 1 20\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
